/*
 * Feistel Cipher
 * @author Pedro Luis
 */
package feistelcipher;

import java.util.ArrayList;
import java.util.HashMap;

public class FeistelCipher {

    private final char[] ALFABETO_I = new char[]{
        // Letras minusculas
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        // Letras mayusculas
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        // Digitos
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        // Otros
        '.', ',', ';', ':', '_', '"', '\'', '¿', '?', '¡', '#', '\\',
        '+', '-', '*', '/', '=', '%', '!', '^', '|', '&', '>', '<',
        '(', ')', '{', '}', '[', ']', ' '};

    private final int LONGITUD = ALFABETO_I.length;
    private int t_bloque = 4;

    private HashMap<Character, Integer> acceso;

    public FeistelCipher() {
        acceso = new HashMap<>();
        iniciarAlfabeto_Acceso();
    }

    private void iniciarAlfabeto_Acceso() {
        for (int i = 0; i < LONGITUD; i++) {
            acceso.put(ALFABETO_I[i], i);
        }
    }

    // Verifica q un caracter pertenezca al alfabeto
    int esta_en_vector_alfabeto(char caracter) {
        Integer res = acceso.get(caracter);
        if (res == null) {
            return -1;
        } else {
            return res;
        }
    }

    // Retorna la pasicion que ocupa de un caracter de cadena en nuestro alfabeto
    int posicion_caracter_alfabeto(char caracter) {
        int i = esta_en_vector_alfabeto(caracter);
        if (i == -1) {
            System.err.println("Caracter :'" + caracter + "' no existe en alfabeto");
        }
        return i;
    }

    // Sustiye un bloque de cadena: llamado a la funcion "sustitucion_caracter" por cada caracter
    String sustituciones_bloque_caracteres(String cadena, int valor) {
        int i = 0,
                len = cadena.length();
        StringBuffer nc = new StringBuffer(cadena.toString());
        int posicion, nueva_posicion, aux;
        while (i < len) {
            posicion = posicion_caracter_alfabeto(cadena.charAt(i));
            aux = posicion + valor;
            if (aux < 0) {
                nueva_posicion = LONGITUD - 1;
            } else {
                nueva_posicion = aux % LONGITUD;
            }

            nc.setCharAt(i, ALFABETO_I[nueva_posicion]);
            i++;
        }
        return nc.toString();
    }

    // Permuta un bloque de cadena: tomas dos caracteres y los intercambia: 
    // llamas a funcion "permutacion_caracteres"
    String permutaciones_bloque_caracteres(String cadena, int op) {
        int len = (int) Math.floor(cadena.length() / 2),
                // Con el tamaño de la cadena realiza la mitad de permutaciones
                k = 0,
                i = 0,
                j = len;
        char[] nc = cadena.toCharArray();
        //char aux;
        if (op == 0 || cadena.length() == 3) {
            while (k < len) {
                char aux = nc[i];
                nc[i] = nc[j];
                nc[j] = aux;
                i = j;
                j++;
                k++;
            }
        } else {
            j = cadena.length() - 1;
            i = j - 1;
            while (k < len) {
                char aux = nc[i];
                nc[i] = nc[j];
                nc[j] = aux;

                i = 0;
                j--;
                k++;
            }
        }
        return new String(nc);
    }

    int numero_bloques(String cadena, int t_bloque) {
        int i = 0,
                bloque_inicio = 0, // Posicion de inicio de bloque cadena
                bloque_final = 0, // Posicion final del bloque de cadena
                len = cadena.length() - 1;
        while (bloque_final != len) {
            bloque_inicio = i * t_bloque;
            if ((bloque_inicio + (t_bloque - 1)) >= len) {
                bloque_final = len;
            } else {
                bloque_final = bloque_inicio + (t_bloque - 1);
            }
            i++;
        }
        return i; // i es el numero de bloques que se pueden crear con la cadena
    }

    // Crea los bloques de cadena: separa la cadena en bloques, cada una con un tamaño de bloque
    ArrayList<String> crear_bloques(String cadena, int n_bloques, int t_bloque, int op) {
        // n_bloques: numero de bloques
        // t_bloque: tamaño de cada bloque de cadena
        // op: indica si lo que se esta realizando es una encriptacion o desencriptacion
        // si op=0: encriptacion y si op=1: desencriptacion
        int i = 0,
                bloque_inicio = 0,
                bloque_final = 0;
        ArrayList<String> bloques = new ArrayList<>();
        int m = n_bloques % 2;
        while (i < n_bloques) {
            if (n_bloques > 1 && op == 1 && i == (n_bloques - 2) && m == 0) {
                int aux = (i + 1) * t_bloque,
                        lon = cadena.substring(aux, cadena.length()).length();

                bloque_inicio = i * t_bloque;
                bloque_final = bloque_inicio + lon;

                bloques.add(cadena.substring(bloque_inicio, bloque_final));
                bloques.add(cadena.substring(bloque_final, cadena.length()));
                break;
            }

            bloque_inicio = i * t_bloque;
            if (i == n_bloques - 1) {
                bloque_final = cadena.length();
            } else {
                bloque_final = bloque_inicio + t_bloque;
            }

            bloques.add(cadena.substring(bloque_inicio, bloque_final));
//            System.out.print(cadena.substring(bloque_inicio, bloque_final) + " ");
            i++;
        }
//        System.out.println();
        return bloques;
    }

    // Realiza las sustituciones de todos bloques de cadena creados
    String sustituciones(ArrayList<String> bloques, int n_bloques, int i_bloque, int valor) {
        // i_bloque: indica el numero de bloque desde donde se comienza a permutar
        // valor: indica encriptacion o desencriptacion
        int i = 0;
        String cadena_sus = "";
        while (i < n_bloques) {
            if (i % 2 == i_bloque) {
                bloques.set(i, sustituciones_bloque_caracteres(bloques.get(i), valor));
            }
            cadena_sus += bloques.get(i);
//            System.out.print(bloques.get(i) + " ");
            i++;
        }
//        System.out.println();
        return cadena_sus;
    }

    // Realiza las permutaciones por cada bloque de cadena creados
    String permutaciones(ArrayList<String> bloques, int n_bloques, int i_bloque, int op) {
        int i = 0;
        String cadena_per = "";
        while (i < n_bloques) {
            if (i % 2 == i_bloque) {
                bloques.set(i, permutaciones_bloque_caracteres(bloques.get(i), op));
            }
            cadena_per += bloques.get(i);
//            System.out.print(bloques.get(i) + " ");
            i++;
        }
//        System.out.println();
        return cadena_per;
    }

    // Intercambia los bloques de cadena: el 1ro con el 2do, el 3ero con el 4to, ...
    void intercambio_bloques(ArrayList<String> bloques, int n_bloques) {
        int k = 0,
                i = 0,
                j = 1,
                len = (int) ((double) n_bloques / (double) 2);
        String aux;
        while (k < len) {
            aux = bloques.get(i);
            bloques.set(i, bloques.get(j));
            bloques.set(j, aux);
            i += 2;
            j += 2;
            k++;
        }
    }

    public String encriptarLinea(String cadena) {
        int n_bloques = numero_bloques(cadena, t_bloque);
        ArrayList<String> bloques = crear_bloques(cadena, n_bloques, t_bloque, 0); // Formamos lo bloques de cadena

        // Proceso de Encriptacion
        int valor = 1; // Valor bandera de encriptacion

        sustituciones(bloques, n_bloques, 0, valor);

        permutaciones(bloques, n_bloques, 0, 0);

        intercambio_bloques(bloques, n_bloques);

        sustituciones(bloques, n_bloques, 0, valor);
        String cadena_encriptada = permutaciones(bloques, n_bloques, 0, 0);

        bloques.clear();

        return cadena_encriptada;
    }

    public String desencriptarLinea(String cadena_encriptada) {
        int n_bloques = numero_bloques(cadena_encriptada, t_bloque);
        ArrayList<String> bloques_encriptado = crear_bloques(cadena_encriptada, n_bloques, t_bloque, 1); // Creadmos los bloques

        // Proceso de Encriptacion
        int valor = -1; // Valor bandera de encriptacion

        permutaciones(bloques_encriptado, n_bloques, 0, 1);
        sustituciones(bloques_encriptado, n_bloques, 0, valor);

        intercambio_bloques(bloques_encriptado, n_bloques);

        permutaciones(bloques_encriptado, n_bloques, 0, 1);
        String cadena_desencriptada = sustituciones(bloques_encriptado, n_bloques, 0, valor);

        bloques_encriptado.clear();

        return cadena_desencriptada;
    }
}
