/*
 * Main 
 * @author Pedro Luis
 */
package feistelcipher;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //<editor-fold defaultstate="collapsed" desc="Look and feel setting code (optional) ">
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            System.err.println("" + ex.getMessage());
        }

        //</editor-fold>
        
        MainFrame frame = new MainFrame();
        frame.setTitle("Feistel Cipher");
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
